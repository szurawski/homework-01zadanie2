package Liczydlo;


public class Zadanie02main {
    public static void main(String[] args) {
        //TWORZE TABLICE PSEUDO LOSOWO
        int[] tablica = new int[10];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = (int) (Math.random() * 21); //20 - zakres liczb losowych
            System.out.println(tablica[i]);
        }
        //LICZE SUME I ILOCZYN
        int j = 0;
        int suma = 0;
        int iloczyn = 1;
        do {
            suma += (tablica[j]);
            iloczyn *= (tablica[j]);
            //System.out.println(tablica[j]);
            j++;
        } while (j < tablica.length);
        System.out.println("Suma wszystkich elementów tablicy wynosi: "+ suma);
        System.out.println("Iloczyn wszystkich elementów tablicy wynosi: "+ iloczyn);

    }
}
